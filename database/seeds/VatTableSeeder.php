<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vat')->insert([
            [
                'title' => '0%',
                'value' => 0.00
            ],
            [
                'title' => '10%',
                'value' => 10.00
            ],
            [
                'title' => '18%',
                'value' => 18.00
            ]
        ]);
    }
}
