<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i< 150; $i++){
            DB::table('products')->insert([
                'user_id' => 1,
                'name' => str_random(10),
                'description' => str_random(15),
                'price_without_vat' => rand(1, 500),
                'vat' => 18.00,
                'price_with_vat' => rand(1,100),
            ]);
        }
    }
}
