<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0;$i<15;$i++){


        DB::table('customers')->insert([
            'user_id' => 1,
            'name' => str_random(7),
            'email' => str_random(10),
            'phone_number' => random_int(100, 99999999),
            'address' => str_random(10),
        ]);

        }
    }
}
