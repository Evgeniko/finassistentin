<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(VatTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(UnitsTableSeeder::class);
    }
}
