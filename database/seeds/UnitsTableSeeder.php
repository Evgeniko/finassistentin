<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            [
                'value' => 'hour',
                'title' => 'hour'
            ],
            [
                'value' => 'day',
                'title' => 'day'
            ]
        ]);
    }
}
