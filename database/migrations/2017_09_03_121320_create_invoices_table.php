<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->integer('user_id');
            $table->string('customer_name');
            $table->text('customer_address');
            $table->string('status')->default('Ожидается');
            $table->json('products');
            $table->text('client_note')->nullable();
            $table->text('client_note_more')->nullable();
            $table->timestamp('date');
            $table->timestamp('due_date');
            $table->string('country')->nullable();
            $table->string('currency');
            $table->decimal('total_sum_without_vat', 19,2);
            $table->decimal('total_sum_with_vat', 19,2);
            $table->decimal('total_vat', 19,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
