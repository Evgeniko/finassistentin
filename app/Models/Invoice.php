<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $guarded = ['id'];

    protected $casts = [
        'products' => 'collection'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','invoices_products', 'product_id', 'invoice_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
}
