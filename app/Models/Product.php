<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id']; //user_id

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function invoices()
    {
        return $this->belongsToMany('App\Models\Invoice', 'invoices_products', 'invoice_id','product_id');
    }
}
