<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    /**
     * Get user statistics
     *
     * @return array
     */
    public function getUserStatistics()
    {
        $user = Auth::user();
        $statistics = [];
        $statistics['productCount'] = $user->products()->count();
        $statistics['invoicesCount'] = $user->invoices()->count();

        return $statistics;
    }
}
