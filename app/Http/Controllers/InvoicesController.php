<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Vat;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Vat $vatTbl)
    {
        $invoices = Auth::user()->invoices()->with('customer')->get();
        $vats = $vatTbl->all();
        return view('invoices.invoices', compact('invoices', 'vats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Vat $vatTbl, Unit $unitTbl)
    {
        $vats = $vatTbl->all();
        $units = $unitTbl->all();
        return view('invoices.create', compact('vats', 'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Invoice $invoice)
    {
        //  dd($request);
        $request = $request->toArray();
        $request['user_id'] = Auth::id();
        $invoice->create($request);
        return redirect(route('invoices'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Auth::user()->invoices()->where('id', $id)->first();
        $invoiceProducts = unserialize($invoice->products);
        return view('invoices.show', compact('invoice', 'invoiceProducts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userInvoices(Invoice $invoiceModel)
    {
        $invoices = $invoiceModel->where('user_id', Auth::id())->with('customer')->get();
        return $invoices;

        //return Auth::user()->with('invoices', 'customers')->get();
    }
}
