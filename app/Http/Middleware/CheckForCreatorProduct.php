<?php

namespace App\Http\Middleware;

use App\Models\Product;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckForCreatorProduct
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $id = $request->id;
        $product = Product::find($id);
        if ($product->user_id !== Auth::id()){
            return response('Product not found', 404);
        }
        return $next($request);
    }
}
