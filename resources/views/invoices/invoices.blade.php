@extends('layouts.app')
@section('content')
    <!--InvoiceNo1-->
    @foreach($invoices as $invoice)
    <div id="view-invoice-{{ $invoice->id }}" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal"><i class="fa fa-window-close"></i></button>
                    <h3 class="modal-title">Invoice No. {{ $invoice->number?:$invoice->id }}</h3>
                </div>
                <div class="modal-body" style="font-size: 15px;">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                    <h2><strong>God ott</strong></h2>
                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-link" data-toggle="dropdown"><h4>More</h4></button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <li><a href="#">Archive</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Change Desing</a></li>
                                        <li><a href="#">Copy</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="Edit Invoices.html">Edit</a></li>
                                        <li class="divider"></li>
                                        <li><a data-toggle="modal" data-target="#Send">Send</a></li>
                                        <li><a href="#">Print</a></li>
                                        <li><a href="#">Download</a></li>
                                        <li class="divider"></li>
                                        <li><a href="New Credit Note.html">Create Credit Note</a></li>
                                        <li><a href="New Delivery Note.html">Create Delivery Note</a></li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="col-lg-12 text-center">
                            <p>
                                {{ $invoice->customer_name }} <br>
                                {{ $invoice->customer_address }}
                            </p>
                        </div>
                        <div class="col-lg-12 text-center">
                            <p>Rechnung</p>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <p>Rechnungsnr.</p>
                                    <p>Rechnungsdatum</p>
                                    <p>Fälligkeitsdatum</p>
                                    <hr>
                                    <p><strong>Zu zahlen EUR</strong></p>
                                    <hr>
                                </div>
                                <div class="col-lg-6">
                                    <p>1</p>
                                    <p>{{ $invoice->date }}</p>
                                    <p>{{ $invoice->due_date }}</p>
                                    <hr>
                                    <p><strong>{{ $invoice->total_sum_with_vat }}</strong></p>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center">
                            <br>
                            <p>{{ $invoice->client_note_more }}</p>
                        </div>
                        <div class="col-lg-12 text-center">
                            <table class="footable table table-bordered table-stripped table-hover table-sm toggle-arrow-tiny">
                                <thead >
                                <tr>
                                    <th>Beschreibung</th>
                                    <th>Menge</th>
                                    <th>Einheit</th>
                                    <th>Preis</th>
                                    <th>Betrag</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($invoice->products as $product)
                                    <tr>
                                        <td>{{ $product['product_name'] }} <br> {{ $product['product_description'] }}</td>
                                        <td>{{ $product['product_quantity'] }}</td>
                                        <td>{{ $product['product_unit'] }}</td>
                                        <td>{{ $product['product_price'] }}</td>

                                    </tr>
                                @endforeach
                                <tr>
                                    <td>Foto sessesion <br> 1h + prost production</td>
                                    <td>1</td>
                                    <td>Stunde(n)</td>
                                    <td>12,61</td>
                                    <td>12,61</td>
                                </tr>
                                <tr>
                                    <td>Albom <br> 30 Sites</td>
                                    <td>1</td>
                                    <td>Stück</td>
                                    <td>4,20</td>
                                    <td>4,20</td>
                                </tr>
                                </tbody>
                            </table><br>
                        </div>
                        <div class="col-lg-12 text-center">
                            <div class="row">
                                <div class="col-lg-6">
                                    <p>Zwischensumme ohne USt.</p>
                                    <p>USt. 19% von 16,81</p>
                                    <p>Gesamt EUR</p>
                                </div>
                                <div class="col-lg-6">
                                    <p>16,81</p>
                                    <p>3,19</p>
                                    <p>20,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center">
                            <br>
                            <p>Thank you for your message</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="Notifications.html" class="btn btn-info btn-block">Enter Payment</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- Send modal-->
    <div id="Send" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal"><i class="fa fa-window-close"></i></button>
                    <h3 class="modal-title">Send Invoice</h3>
                </div>
                <div class="modal-body">
                    <form action="form">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" placeholder="accountant@example.com" value="new@mil.ru">
                            <label style="margin-top: 12px;">Subject</label>
                            <input type="text" class="form-control" placeholder="accountant@example.com" value="Invoice no. 1 from God ottGod ottGod ott">
                        </div>
                        <div class="form-group">
                            <label>Message to Accountant</label>
                            <textarea class="form-control" style="height: 160px; resize: none;" placeholder="Message to Accountant">
Dear John Galt,

To view your invoice for EUR 20,00 click the button below. We also attached a PDF copy of it for your records.

Best regards,
God ott</textarea>
                        </div>
                        <div style="font-size: 15px;">
                            <p class="text-center">A PDF file is automatically attached to your message.</p>
                            <div class="checkbox text-center" style="margin-top: 20px;">
                                <input type="checkbox" class="i-checks MarginTopForm" value="Viewed"><br> Send a copy to myself
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-info btn-block">Send</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="well">
            <div class="row MarginTop_2 text-center">
                <div class="col-xs-12 col-sm-12 col-lg-2">
                    <a href="{{ route('invoiceCreate') }}" class="btn btn-primary btn-block">New Invoice</a><br>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-8">
                    <form action="form" class="form-search">
                        <div class="input-group form-group">
                            <input type="text" placeholder="Search" class="input-md form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary " style="padding: 7px 15px;"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-2">
                    <button class="btn btn-warning btn-block" data-toggle="collapse" data-target="#Filter">Filter</button>
                </div>
            </div>
            <div class="collapse text-center" id="Filter">
                <form action="form" role="form" class="well form-inline">
                    <div class="form-group col-xs-12 col-sm-6 col-lg-3" id="data_1"><br>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" placeholder="From">
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-lg-3" id="data_1"><br>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" placeholder="To">
                        </div>
                    </div>
                    <div class="checkbox col-xs-12 col-sm-2 col-lg-1"><br>
                        <input type="checkbox" class="i-checks MarginTopForm" value="Draft"><br>Draft
                    </div>
                    <div class="checkbox col-xs-12 col-sm-2 col-lg-1"><br>
                        <input type="checkbox" class="i-checks MarginTopForm" value="Sent"><br>Sent
                    </div>
                    <div class="checkbox col-xs-12 col-sm-4 col-lg-1"><br>
                        <input type="checkbox" class="i-checks MarginTopForm" value="Viewed"><br>Viewed
                    </div>
                    <div class="checkbox col-xs-12 col-sm-2 col-lg-1"><br>
                        <input type="checkbox" class="i-checks MarginTopForm" value="Unpaid"><br>Unpaid
                    </div>
                    <div class="checkbox col-xs-12 col-sm-2 col-lg-1"><br>
                        <input type="checkbox" class="i-checks MarginTopForm" value="Paid"><br>Paid
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn btn-info" style="width: 200px; margin-top: 30px;">Seve Filter</a>
                    </div>
                </form>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Invoices</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                    </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="invoices-table" class="footable table table-bordered table-stripped table-hover table-sm toggle-arrow-tiny">
                                    <thead >
                            <tr>
                                <th>Status</th>
                                <th>No.</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Dau Date</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                                    <tbody>
                            @foreach($invoices as $invoice)
                                <tr>
                                    <td data-toggle="modal" data-target="#view-invoice-{{ $invoice->id }}">{{ $invoice->status }}</td>
                                    <td data-toggle="modal" data-target="#view-invoice-{{ $invoice->id }}">{{ $invoice->number?:$invoice->id }}</td>
                                    <td data-toggle="modal" data-target="#view-invoice-{{ $invoice->id }}">{{ $invoice->customer_name }}</td>
                                    <td data-toggle="modal" data-target="#view-invoice-{{ $invoice->id }}"><span class="pie">{{ $invoice->date }}</span></td>
                                    <td data-toggle="modal" data-target="#view-invoice-{{ $invoice->id }}">{{ $invoice->due_date }}</td>
                                    <td>
                                        <div class="dropdown">{{ $invoice->total_sum_with_vat }}<button type="button" class="btn-link dropdown-toggle" data-toggle="dropdown"><span class="fa fa-chevron-circle-down"></span></button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                <li><a data-toggle="modal" data-target="#InvoiceNo1">Open</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Archive</a></li>
                                                <li><a href="#">Enter Payment</a></li>
                                                <li class="divider"></li>
                                                <li><a data-toggle="modal" data-target="#Send">Send</a></li>
                                                <li><a href="#">Print</a></li>
                                                <li><a href="#">View PDF</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Copy</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Status.</th>
                                            <th>No.</th>
                                            <th>Customer</th>
                                            <th>Date</th>
                                            <th>Dau Date</th>
                                            <th>Amount</th>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="text-center"><strong> {{ $invoices->count() }} Invoices</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="text-center"><strong> Total amount: 20,00. Unpaid amount: 20,00</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection