@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            Счёт №{{ $invoice->id }}
        </div>
        <div class="card-content">
            <h1>{{ $invoice->description }}</h1>
            Продукт/Услуга:
            <br>
            Имя: {{ $invoiceProducts['product_id'] }}
        </div>
    </div>
@endsection