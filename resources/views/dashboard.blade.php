@extends('layouts.app')
@section('content')

        <div class="row">
            <div class="col-lg-12">
                <div class="well">
                    <div class="row text-center">
                        <div class="col-xs-12 col-lg-12">
                            <h1>Debitoor + iZettle</h1>
                            <p>Get an iZettle card reader to accept payments on the spot and have all transactions in Debitoor automatically.</p>
                            <a href="Receive Payment.html" class="btn btn-info">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-center">
                <div class="well">
                    <br><h1>Bank & Cash</h1><br>
                    <button class="btn btn-info" data-toggle="collapse" data-target="#NewBank">Add an account</button><br><br>
                    <div class="collapse" id="NewBank">
                        <div class="well">
                            <form action="form">
                                <div class="form-group">
                                    <label>Name of your bank</label>
                                    <input id="NameBankID" type="text" class="form-control text-center" placeholder="Name bank">
                                </div>
                            </form>
                            <button id="NextBankID" disabled="disabled" class="btn btn-info" data-toggle="modal" data-target="#NextBank" style="width: 200px;">Next</button><br><br>
                            <p>Create a manual account if you want to enter your transactions one by one instead of importing them.</p>
                            <button class="btn btn-link" data-toggle="collapse" data-target="#CreateManualAccount">Create manual account</button>
                            <div class="collapse" id="CreateManualAccount">
                                <form action="form" class="well">
                                    <div class="form-group">
                                        <label class="text-center">Name</label>
                                        <input type="text" class="form-control text-center" placeholder="Name"><br>
                                        <label class="text-center">Balance</label>
                                        <input type="text" class="form-control text-center" placeholder="Balance">
                                    </div>
                                    <a href="#" class="btn btn-info">Add manual account</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="well">
                    <h1 class="text-center">Income</h1>
                    <div class="ibox-content">
                        <div>
                            <div id="pie"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="well">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center">Income & Expenses</h1>
                            <div class="ibox-content">
                                <div>
                                    <canvas id="barChart" height="100"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h1 class="text-center">Expenses</h1>
                            <div class="ibox-content">
                                <div>
                                    <canvas id="doughnutChart" height="100"></canvas>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div>
                                    <canvas id="lineChart" height="0" style="display: none"></canvas>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="text-center">
                                    <canvas id="polarChart" height="140" style="display: none;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
