<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    {{--inspinia plugins--}}
    <link href="{{ asset('/css/inspinia/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/inspinia/plugins/c3/c3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/inspinia/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/inspinia/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/inspinia/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/inspinia/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/inspinia/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <div id="app">
        @if(Auth::guest())
            @include('layouts.noAuth.navbar')
            @yield('content')
        @else
            @include('layouts.sidebar')
            <div id="page-wrapper" class="gray-bg">
                @include('layouts.navbar')
                <div class="wrapper wrapper-content animated fadeInRight">
                    @yield('content')
                </div>
            </div>
        @endif
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
    {{--inspinia plugins--}}
    <script src="{{ asset('/js/inspinia/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('/js/inspinia/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('/js/inspinia/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('/js/inspinia/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('/js/inspinia/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/inspinia.js') }}"></script>
    <script src="{{ asset('/js/inspinia/pace/pace.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/gritter/jquery.gritter.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('/js/inspinia/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/js/inspinia/typehead/bootstrap3-typeahead.min.js') }}"></script>
    {{--my scripts--}}
    <script src="{{ asset('/js/init.js') }}"></script>
    <script src="{{ asset('/js/invoices.js') }}"></script>
    <script src="{{ asset('/js/input-only-number.js') }}"></script>
    <script src="{{ asset('/js/input-only-integer.js') }}"></script>
    <script src="{{ asset('/js/percent-calculate.js') }}"></script>
    <script src="{{ asset('/js/ajaxas/confirm-delete.js') }}"></script>
    <script src="{{ asset('/js/ajaxas/editProductSave.js') }}"></script>
    <script src="{{ asset('/js/ajaxas/newProductSave.js') }}"></script>
    <script src="{{ asset('/js/complete-invoice.js') }}"></script>
</body>
</html>
