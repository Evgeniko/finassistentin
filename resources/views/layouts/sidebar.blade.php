<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img src="img/profile_small.jpg" alt="" class="img-circle">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                            </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="Notifications.html">Notifications</a></li>
                        <li><a href="Company.html">Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Log out</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="active">
                <a href="{{ route('home') }}"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-line-chart"></i> <span class="nav-label">Invoices</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('invoices') }}">Invoice Table</a></li>
                    <li><a href="#"> Other Income</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-quote-right"></i> <span class="nav-label">Quotes</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="Quotes Table.html"> Quotes Table</a></li>
                    <li><a href="Delivery Notes.html"> Delivery Notes</a></li>
                </ul>
            </li>
            <li>
                <a href="Expenses.html"><i class="fa fa-bar-chart-o"></i><span class="nav-label">Expenses</span></a>
            </li>
            <li>
                <a href="Banking.html"><i class="fa fa-briefcase"></i> <span class="nav-label">Banking</span> </a>
            </li>
            <li>
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Contacts</span> </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('customers') }}"> Customers</a></li>
                    <li><a href="Suppliers.html"> Suppliers</a></li>
                    <li><a href="Accountant.html"> Accountant</a></li>
                    <li><a href="Team.html"> Team</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('products') }}"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Products</span> </a>
            </li>
            <li>
                <a href="#"><i class="fa fa-cogs"></i><span class="nav-label">Settings</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="Company.html">Company</a></li>
                    <li><a href="Plans & Pricing.html">Plans & Pricing</a></li>
                    <li><a href="Account.html">Account</a></li>
                    <li><a href="Email Templates.html">Email Templates</a></li>
                    <li><a href="Receive Payment.html">Receive Payment</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>