<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" method="post" action="#">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right FontSize">
            <li>
                <a class="btn btn-link" data-toggle="modal" data-target="#Notifications" style="font-size: 12px;">
                    <i class="fa fa-envelope"></i>Notify <span class="badge">0</span>
                </a>
            </li>
            <li>
                <a href="#" class="btn btn-link" style="font-size: 12px;">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>

<!-- Notifications modal-->
<div id="Notifications" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal"><i class="fa fa-window-close"></i></button>
                <h3 class="modal-title">Notifications</h3>
            </div>
            <div class="modal-body text-center" style="font-size: 15px;">
                <p>Nothing just yet.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-info btn-block">See All</a>
            </div>
        </div>
    </div>
</div>

<!--NextBank-->
<div id="NextBank" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal"><i class="fa fa-window-close"></i></button>
                <h3 class="modal-title">Add Bank Account</h3>
            </div>
            <div class="modal-body">
                <div class="well text-center">
                    <form action="form">
                        <h3>Connect to your bank</h3>
                        <p>Verbinde deine Bankkonten mit Debitoor via figo - Login Daten erhältst du von figo, sobald du dein Konto angelegt hast. Falls du noch kein Konto hast, kannst du dich kostenlos here anmelden.</p><br>
                        <div class="form-group">
                            <label>Username</label>
                            <input  type="text" class="form-control" placeholder="Username"><br>
                            <label>Password</label>
                            <input  type="password" class="form-control" placeholder="Password">
                        </div>
                    </form>
                    <br>
                    <button id="Connect" class="btn btn-info btn-block">Connect</button>
                </div>
                <div class="well text-center">
                    <h3>Upload a bank statement</h3>
                    <p>Upload a CSV file of your bank transactions. You can keep your transaction list in Debitoor updated by regularly importing your latest bank statement.</p><br>
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="..."/>
                                </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                    <br>
                    <button id="Upload" class="btn btn-info btn-block">Upload</button>
                </div>
            </div>
        </div>
    </div>
</div>