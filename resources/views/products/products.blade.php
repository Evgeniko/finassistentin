@extends('layouts.app')
@section('content')
    <!--New Product-->
    <div id="new-product-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <form id="new-product-form" action="{{ route('productSave') }}" role="form" method="POST">
                {!! csrf_field() !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button id="CloseModal" class="close" data-dismiss="modal"><i class="fa fa-window-close"></i></button>
                    <h3 class="modal-title" id="NewProduct">New Product</h3>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group text-left">
                                <label>
                                    Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="name" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Product No.</label>
                                <input type="text" name="number" class="form-control" placeholder="Product No.">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>
                                    Net Price (without VAT)
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="input-only-number form-control limitInput" id="price_without_vat" name="price_without_vat" placeholder="Net Price (without VAT)" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" style="height: 250px;  resize: none;" placeholder="Description"></textarea>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>VAT
                                    <span class="text-danger">*</span>
                                </label>
                                <select name="vat" class="form-control" id="vat" name="vat" required>
                                    @foreach($vats as $vat)
                                        <option value="{{ $vat->value }}">{{ $vat->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Gross Price (with VAT)
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="input-only-number form-control limitInput" id="price_with_vat" name="price_with_vat" placeholder="Gross Price (with VAT)" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Unit</label>
                                <div>
                                    <select name="unit" class="chosen-select form-control"  tabindex="2">
                                        <option value="None">None</option>
                                        <option value="hour">hour</option>
                                        <option value="day">day</option>
                                        <option value="month">month</option>
                                        <option value="each" id="UnitEach">each</option>
                                        <option value="carton">carton</option>
                                        <option value="kg">kg</option>
                                        <option value="km">km</option>
                                        <option value="liter">liter</option>
                                        <option value="meter">meter</option>
                                        <option value="m2">m2</option>
                                        <option value="m3">m3</option>
                                        <option value="night">night</option>
                                        <option value="ifm">ifm</option>
                                        <option value="package">package</option>
                                        <option value="flat rate">flat rate</option>
                                        <option value="ton">ton</option>
                                        <option value="year">year</option>
                                        <option value="minute">minute</option>
                                        <option value="fm">fm</option>
                                        <option value="lm">lm</option>
                                        <option value="word">word</option>
                                        <option value="assignment">assignment</option>
                                        <option value="character">character</option>
                                        <option value="box">box</option>
                                        <option value="bottle">bottle</option>
                                        <option value="bag">bag</option>
                                        <option value="ha">ha</option>
                                        <option value="Ar">Ar</option>
                                        <option value="week">week</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Cost Price</label>
                                <input name="cost_price" type="text" class="input-only-number form-control" placeholder="Cost Price">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button id="new-product-save" href="#" class="btn btn-info btn-block">Add</button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <!--Edit product-->
    @foreach($products as $product)
    <div id="product-edit-{{ $product->id }}" class="modal fade product-edit-modal" tabindex="-1">
        <div class="modal-dialog modal-md">
            <form class="edit-product-form" action="{{ route('productEdit', ['id' => $product->id]) }}" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="CloseModal" class="close" data-dismiss="modal"><i class="fa fa-window-close"></i></button>
                    <h3 class="modal-title">Edit {{ $product->name }}</h3>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group text-left">
                                <label>Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="name" type="text" class="form-control" placeholder="Name" value="{{ $product->name }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Product No.</label>
                                <input name="number" type="text" class="form-control" placeholder="Product No." value="{{ $product->number }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Net Price (without VAT)
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="price_without_vat" type="text" class="input-only-number form-control limitInput" id="price_without_vat" placeholder="Net Price (without VAT)" value="{{ $product->price_without_vat }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" style="height: 250px;  resize: none;" placeholder="Description">{{ $product->description }}</textarea>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>VAT
                                    <span class="text-danger">*</span>
                                </label>
                                <select name="vat" class="form-control" id="vat" required>
                                    @foreach($vats as $vat)
                                        <option value="{{ $vat->value }}" {{ $product->vat == $vat->value ? 'selected':'' }}>{{ $vat->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Gross Price (with VAT)
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="price_with_vat" type="text" class="input-only-number form-control limitInput" id="price_with_vat" placeholder="Gross Price (with VAT)" value="{{ $product->price_with_vat }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Unit</label>
                                <div>
                                    <select class="chosen-select form-control" tabindex="2">
                                        <option value="None">None</option>
                                        <option value="hour">hour</option>
                                        <option value="day">day</option>
                                        <option value="month">month</option>
                                        <option value="each" selected="each">each</option>
                                        <option value="carton">carton</option>
                                        <option value="kg">kg</option>
                                        <option value="km">km</option>
                                        <option value="liter">liter</option>
                                        <option value="meter">meter</option>
                                        <option value="m2">m2</option>
                                        <option value="m3">m3</option>
                                        <option value="night">night</option>
                                        <option value="ifm">ifm</option>
                                        <option value="package">package</option>
                                        <option value="flat rate">flat rate</option>
                                        <option value="ton">ton</option>
                                        <option value="year">year</option>
                                        <option value="minute">minute</option>
                                        <option value="fm">fm</option>
                                        <option value="lm">lm</option>
                                        <option value="word">word</option>
                                        <option value="assignment">assignment</option>
                                        <option value="character">character</option>
                                        <option value="box">box</option>
                                        <option value="bottle">bottle</option>
                                        <option value="bag">bag</option>
                                        <option value="ha">ha</option>
                                        <option value="Ar">Ar</option>
                                        <option value="week">week</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Cost Price</label>
                                <input name="cost_price" type="number" class="form-control" placeholder="Cost Price" id="CostPrice" value="0">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button href="#" class="btn btn-info btn-block">Save</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    @endforeach
    <div class="row">
        <div class="well">
            <div class="row MarginTop_2 text-center">
                <div class="col-xs-12 col-sm-12 col-lg-2">
                    <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#new-product-modal">New Product</button> <br>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-2">
                    <button class="btn btn-info btn-block" data-toggle="collapse" data-target="#Import">Import</button> <br>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-8">
                    <form action="" class="form-search">
                        <div class="input-group form-group">
                            <input type="text" placeholder="Search" class="input-md form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary " style="padding: 7px 15px;"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="collapse text-center" id="Import">
                <div class="well">
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="..."/>
                                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <button class="btn btn-info btn-block">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Products</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content ">
                <div class="table-responsive">
                    <table id="product-table" class="footable table table-bordered table-stripped table-hover table-sm toggle-arrow-tiny">
                        <thead >
                    <tr>
                        <th class="hidden">id</th>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>del</th>
                    </tr>
                    </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td class="hidden">{{ $product->id }}</td>
                                    <td data-toggle="modal" data-target="#product-edit-{{ $product->id }}">{{ $product->number?:$product->id }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>{{ $product->price_with_vat }}</td>
                                    <td>
                                        {{--<form action="{{ route('productDelete',['id' => $product->id]) }}" class="delete-product-form" method="POST">--}}
                                            <button class="btn-link" onclick="deleteProductForm(this)" data-id="{{ $product->id }}"><span class="fa fa-trash" style="font-size: 17px;"></span></button>
                                        {{--</form>--}}

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>del</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection