 $(document).ready(function() 
 {
  var PriceFormatValue = $('#PriceFormat option:selected').val();
  var N = 1;
  var QuantityVal = parseFloat($('#Quantity').val());
  var PriceVal = parseFloat($('#Price').val());

  $('#PriceFormat').click(function(){
    if($('#PriceFormat option:selected').val() == 'NET'){
      N = 0;
      N = N + 1;
      $('.NetAndGross').html('(net)');
      $('#WithoutVAT').html('');  
    }
    if($('#PriceFormat option:selected').val() == 'GROSS') { 
      N = 0;
      N = N + 2;
      $('.NetAndGross').html('(gross)');
      $('#WithoutVAT').html('without VAT');    
    }
  });
  $('#CurrencyID').click(function(){
    $('#CurrencyValue').html($('#CurrencyID option:selected').val());
  });
  $('#VAT').click(function(){
    $('#VatVal').html($('#VAT option:selected').val());
  });


  var KL = 0;
  $('#DiscountName').click(function(){
    KL = KL + 1;
    if (KL == 1) {
     $('#DisName').html('Remove Discount');
   } else if (KL == 2){
     $('#DisName').html('Discount');
     KL = 0;
     $('#DiscountVal_2').val('');
   }
 });

  var OL = 0;
  $('#BtnOptions').click(function(){
    OL = OL + 1;
    if (OL == 1) {
     $('#BtnOptions').html('Hide options');
   } else if (OL == 2){
     $('#BtnOptions').html('Show more options');
     OL = 0;
   }
 });

// VAT
$('#VAT').click(function(){
if(N == 1){  
   QuantityVal = parseFloat($('#Quantity').val());
   PriceVal = parseFloat($('#Price').val());
   if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
} else if(N == 2){
  QuantityVal = parseFloat($('#Quantity').val());
  PriceVal = parseFloat($('#Price').val());
  if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {    
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
}
});

// PriceFormat
$('#PriceFormat').click(function(){
if(N == 1){  
   QuantityVal = parseFloat($('#Quantity').val());
   PriceVal = parseFloat($('#Price').val());
   if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
} else if(N == 2){
  QuantityVal = parseFloat($('#Quantity').val());
  PriceVal = parseFloat($('#Price').val());
  if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {    
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
}
});



// DiscountVal

$('#DiscountVal').keyup(function(){
  if(N == 1){  
   QuantityVal = parseFloat($('#Quantity').val());
   PriceVal = parseFloat($('#Price').val());
   if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
} else if(N == 2){
  QuantityVal = parseFloat($('#Quantity').val());
  PriceVal = parseFloat($('#Price').val());
  if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {    
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
}

});


// DiscountVal_2

$('#DiscountVal_2').keyup(function(){
  if(N == 1){  
   QuantityVal = parseFloat($('#Quantity').val());
   PriceVal = parseFloat($('#Price').val());
   if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
} else if(N == 2){
  QuantityVal = parseFloat($('#Quantity').val());
  PriceVal = parseFloat($('#Price').val());
  if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {    
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
}

});


//Quantity


$('#Quantity').keyup(function(){
  if(N == 1){  
   QuantityVal = parseFloat($('#Quantity').val());
   PriceVal = parseFloat($('#Price').val());
   if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
} else if(N == 2){
  QuantityVal = parseFloat($('#Quantity').val());
  PriceVal = parseFloat($('#Price').val());
  if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {    
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
}
});


//Price
$('#Price').keyup(function(){
  if(N == 1){  
   QuantityVal = parseFloat($('#Quantity').val());
   PriceVal = parseFloat($('#Price').val());
   if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2));
      Res = Res + ResVAT
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#VATVAL').html('0.00')
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
} else if(N == 2){
  QuantityVal = parseFloat($('#Quantity').val());
  PriceVal = parseFloat($('#Price').val());
  if (KL == 1) {
    var DisVarStart = parseFloat($('#DiscountVal_2').val());
    var Res = QuantityVal * PriceVal;
    var DisVar = Res * DisVarStart;
    DisVar =  DisVar / 100;
    Res = Res - DisVar;
  } else {
    var Res = QuantityVal * PriceVal;
  }
  $('#AmountResult').val(Res.toFixed(2));
  var DiscountValValue = $('#DiscountVal').val();
  if(DiscountValValue == '') {
    $('#OfVat').html(Res.toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = Res * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '7%') {
      ResVAT = Res * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
    };
  } else {
    Dis = Res * DiscountValValue;
    Dis = Dis / 100;
    $('#OfVat').html((Res - Dis).toFixed(2));
    $('#SubtotalVal').html(Res.toFixed(2));
    $('#DiscountVla').html('-' + Dis.toFixed(2));
    var VatQ = $('#VAT option:selected').val();
    if (VatQ == '19%') {
      ResVAT = (Res - Dis) * 19;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '7%') {    
      ResVAT = (Res - Dis) * 7;
      ResVAT = ResVAT / 100;
      $('#VATVAL').html(ResVAT.toFixed(2))
      $('#OfVat').html((Res - ResVAT).toFixed(2));
      $('#SubtotalVal').html((Res - ResVAT).toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    } else if (VatQ == '0%') {
      $('#OfVat').html(Res.toFixed(2));
      $('#VATVAL').html('0.00')
      $('#SubtotalVal').html(Res.toFixed(2));
      $('#TotalValID').html(Res.toFixed(2));
      $('#TotalValID').html((Res - Dis).toFixed(2));
    };
  }
}
});



});