<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home/get-user-statistics', 'HomeController@getUserStatistics');

    Route::get('/invoices', 'InvoicesController@index')->name('invoices');
    Route::get('/invoice/create', 'InvoicesController@create')->name('invoiceCreate');
    Route::post('/invoice/save', 'InvoicesController@store')->name('invoiceSave');
    Route::get('/invoice/{id}', 'InvoicesController@show')->name('invoice');
    Route::get('/user-invoices', 'InvoicesController@userInvoices');

    Route::get('/products', 'ProductsController@index')->name('products');
    Route::post('/product/save', 'ProductsController@store')->name('productSave');
    Route::post('/products/edit', 'ProductsController@update')->name('productEdit')->middleware('productCreator');
    Route::post('/product/delete/{id}', 'ProductsController@destroy')->name('productDelete')->middleware('productCreator');
    Route::get('/user-products', 'ProductsController@userProducts');


    Route::get('/customers', 'CustomersController@index')->name('customers');
    Route::get('/user-customers', 'CustomersController@userCustomers');

    Route::get('get-vat', 'VatController@all');


});
