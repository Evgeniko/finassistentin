/** Customers and Date **/
$.get('/user-customers', function (data) {
    $('#customer-typeahead').typeahead({
        source: data,
        fitToElement: true,
        afterSelect: function (selected) {
            $("textarea[name='customer_address']").val(selected.address);
        }
    }, 'json');
});

/** Invoice Lines **/
$.get('/user-products', function (data) {
    $('.products-typeahead').typeahead({
        source: data,
        fitToElement: true,
        afterSelect: function (selected) {
            var line = $(this.$element).closest('.invoice-line');
            //var vat = line.find("select[name='vat']");
            //var product_price = line.find("input[name='product_price']");
            //
            var quantity = getQuantity(line);
            //Вставка данных в поля при выборе продукта
            line.find(".product-price-line").val(selected.price_without_vat);
            line.find(".product-quantity-line").val(quantity);
            line.find(".product-description-line").val(selected.description);
            line.find(".product-vat-line").val(selected.vat);

            calculateAmountInput(line);
        }
    })
});

function calculateAmountInput(line) {
    var price = line.find('.product-price-line').val();
    var quantity = getQuantity(line);

    var amount = ( parseFloat(price) * parseInt(quantity) ).toFixed(2);
    line.find('.product-amount-line').val(amount);

    var totalSumWithoutVat = invoice.totalSumWithoutVat.get();
    invoice.totalSumWithoutVat.set(totalSumWithoutVat);

    var totalVat = invoice.totalVat.get();
    invoice.totalVat.set(totalVat);

    var totalSumWithVat = invoice.totalSumWithVat.get();
    invoice.totalSumWithVat.set(totalSumWithVat);

    invoice.ofWithoutVat();

}

function getQuantity(line) {
    var quantity = line.find(".product-quantity-line").val();
    if (!quantity) {
        line.find(".product-quantity-line").val(1);
        return 1;
    }
    return quantity;
}

$('.product-quantity-line').on('change', function () {
    var line = $(this).closest('.invoice-line');
    calculateAmountInput(line);
});

$('.product-price-line').on('change', function () {
    var line = $(this).closest('.invoice-line');
    calculateAmountInput(line);
});

$('.product-vat-line').on('change', function () {
    var line = $(this).closest('.invoice-line');
    calculateAmountInput(line);
});

$('#add-invoice-line').on('click', function () {
   var invoiceLines = $('.invoice-lines');
   invoiceLines.append('');
});

/** Totals **/
/*function setTotalWithoutVat(){
    /!*var totalWithoutVat = 0;
    var amounts = $('.invoice-lines').find($('.product_amount'));
    amounts.each(function () {
        totalWithoutVat =(parseFloat(totalWithoutVat) + parseFloat(this.value)).toFixed(2);
    });
    if (!totalWithoutVat){ totalWithoutVat = 0.00}*!/
    //totalWithoutVat = invoice.sumTotalWithoutVat();
    //$('#total-without-vat').html(totalWithoutVat);
}

function setTotalVat() {
    var totalVat = 0;
    var vats =  $('.invoice-lines').find($('.product-vat-select'));
    vats.each(function () {
        totalVat = ( parseFloat(totalVat) + parseFloat(this.val()) ).toFixed(2);
    });
   if (!totalVat){ totalVat = 0.00 }
   $('#total-vat').html(totalVat);
}
function getAllTotalVat() {
    var allTotalVat = 0;
    var prices = $('.invoice-lines').find($('.product-price-input'));
    prices.each(function () {
        allTotalVat = ( parseFloat() + parseFloat() ).toFixed(2);
    });
    if (!allTotalVat){ allTotalVat = 0.00 }
    $('#total-vat').html(allTotalVat);
}
function setDiscount() {

}*/

var invoice = {

    totalSumWithoutVat: {
        get: function () {
            var totalWithoutVat = 0;
            var amounts = $('.invoice-lines').find($('.product-amount-line'));
            amounts.each(function () {
                totalWithoutVat =(parseFloat(totalWithoutVat) + parseFloat($(this).val())).toFixed(2);
            });
            if (!totalWithoutVat){ totalWithoutVat = 0.00 }
            return totalWithoutVat;
        },
        set: function (val) {
            $('#total-without-vat').html(val);
            $("input[name='total_sum_without_vat']").val(val);
        }
    },
    
    totalVat: {
        get: function () {
            var totalVat = 0;
            var lines = $('.invoice-lines').find($('.invoice-line'));
            lines.each(function () {
                var price = $(this).find($('.product-amount-line'));
                var vat = $(this).find($('.product-vat-line'));
                totalVat = ( parseFloat(price.val()) * parseFloat(vat.val()) / parseInt(100) ).toFixed(2);
            });
            if (!totalVat){ totalVat = 0.00 }
            return totalVat;
        },
        set: function (val) {
            $('#total-vat').html(val);
            $("input[name='total_vat']").val(val);
        }
    },

    totalSumWithVat: {
        get: function () {
            var totalSumWithVat = 0;
            var lines = $('.invoice-lines').find($('.invoice-line'));
            lines.each(function () {
                var priceLine = $(this).find($('.product-amount-line'));
                var vatLine = $(this).find($('.product-vat-line'));
                totalSumWithVat = (parseFloat(totalSumWithVat) + (parseFloat(priceLine.val()) + parseFloat(priceLine.val()) * ( parseFloat(vatLine.val()) / parseInt(100)) ) ).toFixed(2);
            });
            if (!totalSumWithVat) { totalSumWithVat = 0.00 }
            return totalSumWithVat;
        },
        set: function (val) {
            $('#total-sum-with-vat').html(val);
            $("input[name='total_sum_with_vat']").val(val);
        }
    },

    ofWithoutVat: function() {
        val = this.totalSumWithoutVat.get();
        $('#of-without-vat').html(val);
    }

};


