////////////////////////////////New products
/*
$('#price_without_vat').on('change', function () {
    console.log($(this).closest('form'));
    var vat = getVat();
    var value = parseFloat(this.value);
    var result = value+value*(vat/100);
    $('#price_with_vat').val(result.toFixed(2));
});

$('#price_with_vat').on('change', function () {
    var vat = getVat();
    var value = parseFloat(this.value);
    var result = value/(1+vat/100);
    $('#price_without_vat').val(result.toFixed(2));
});
function getVat() {
    return parseFloat($('#vat').val());
};
$('#vat').on('change', function(){
    var vat = parseFloat(this.value);
    var value = parseFloat($('#price_without_vat').val());
    var result = value+value*(vat/100);
    if (!result) { result  = 0};
    $('#price_with_vat').val(result.toFixed(2));
});

*/

////////////////////////////////////////////Edit Products


//Событие на установке цены без прцента
$("input[name='price_without_vat']").on('change', function () {
    var $form = $(this).closest('form'); //находим форму на которой вызввалось событие
    var vat = getVat($form); // получаем выбранный НДС из select
    var value = this.value; //получаем текущее значение

    ///var resul = value+value*(vat/100);

    // высчитываем стоимость с НДС
    var result = ( parseFloat(value) + parseFloat(value) * ( parseFloat(vat) / parseInt(100) ) ).toFixed(2);
    // если полчилось неккоректное выражение, то ставим 0.00
    if (!parseFloat(result)) { result  = (0.00).toFixed(2) }
    // записываем получившиеся значение в поле(цена с ндс)
    $form.find("input[name='price_with_vat']").val(result);

    // округляем текущее значение до двух знаков после запятой и обновляем текущее значение
    var newVal = parseFloat(this.value).toFixed(2);
    if(!parseFloat(newVal)) { newVal = parseFloat(0.00).toFixed(2)}
    this.value = newVal;
});

//Событие на установке цены с процентом
$("input[name='price_with_vat']").on('change', function () {
    var $form = $(this).closest('form');//находим форму на которой вызввалось событие
    var vat = getVat($form);// получаем выбранный НДС из select
    var value = this.value;//получаем текущее значение

   //var result = value/(1+vat/100);
    // высчитываем стоимость без НДС
    var result = ( parseFloat(value) / (parseInt(1) + parseFloat(vat) / parseInt(100) ) ).toFixed(2);
    // если полчилось неккоректное выражение, то ставим 0.00
    if (!parseFloat(result)) { result  = (0.00).toFixed(2) }
    // записываем получившиеся значение в поле (цена без ндс)
    $form.find("input[name='price_without_vat']").val(result);

    // округляем текущее значение до двух знаков после запятой и обновляем текущее значение
    var newVal = parseFloat(this.value).toFixed(2);
    if(!parseFloat(newVal)) { newVal = parseFloat(0.00).toFixed(2)}
    this.value = newVal;
});

// Событие выбора процента
$("select[name='vat']").on('change', function(){
    var $form = $(this).closest('form');//находим форму на которой вызввалось событие
    var vat = this.value;//получаем текущее процент
    var value = $form.find("input[name='price_without_vat']").val();//Ищем цену без процента
    //var result = value+value*(vat/100);

    //Высчитываем на основе цены без НДС и  нового процента, записываем новое значение в аполе с НДС
    var result = ( parseFloat(value) + parseFloat(value) * ( parseFloat(vat) / parseInt(100) ) ).toFixed(2);
    if (!parseFloat(result)) { result  = (0.00).toFixed(2) }
    $form.find("input[name='price_with_vat']").val(result);
});

// Функция получения текущего процента
function getVat($form) {
    var vat = $form.find("select[name='vat']").val(); // находим select с процентом на текущем поле
    return parseFloat(vat); // возвращаем текущее значение процента
}