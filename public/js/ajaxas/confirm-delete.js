/*$("form.delete-product-form").submit(function (e) {
    var $form = $(this);
    swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize()
        }).done(function (data) {
            swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            );
            $('#product-table').DataTable().row($form.closest('tr')).remove().draw();
        }).fail(function () {
            swal(
                'Error',
                'error while deleting the product',
                'error'
            )
        });
    });
    e.preventDefault();
});*/
function deleteProductForm(btn){
    var id = $(btn).attr('data-id');
    swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: 'post',
            url: '/product/delete/'+ id
        }).done(function (data) {
            swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            );
            $('#product-table').DataTable().row(btn.closest('tr')).remove().draw(false);
        }).fail(function () {
            swal(
                'Error',
                'error while deleting the product',
                'error'
            )
        });
    });
}


/*$("form.delete-product-form").submit(function(e) {
    var $form = $(this);
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize()
        }).done(function (data) {
            console.log(data);
            swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }).fail(function (e) {
            swal(
                'Error',
                'error while deleting the product',
                'error'
            )
        });
    });
    //отмена действия по умолчанию для кнопки submit
    e.preventDefault();
});*/
