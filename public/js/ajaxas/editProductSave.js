$(document).ready(function () {
    $("form.edit-product-form").submit(function(e) {
        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize()
        }).done(function(response) {
            $('.product-edit-modal').modal('hide');

            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Updated');


        }).fail(function(e) {
            console.log(e);
        });
        //отмена действия по умолчанию для кнопки submit
        e.preventDefault();
    });
});
